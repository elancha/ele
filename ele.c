// @C{Copyright notice}

// Copyright 2022 Ernesto Lanchares <elancha98 at gmail.com>.

// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.

// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

// @t{ELE}
// Here's a description of ele

// @s{Includes}
// We have to have this section at the start because we are using @ref{ELE} in
// source code mode.
#include<stdio.h>

// @s{Another section}
void
print_chunk(void)
{

}
// @s{Main}
// this is the main section and the heart of ele. Please take a look at variable
// @code{onevar}
int
main(int argc, char *argv[])
{
	int onevar = 0;
	print_chunk();


	onevar = 2;
	return onevar;
}
