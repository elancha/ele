// @ Copyright notice

// Copyright 2022 Ernesto Lanchares <elancha98 at gmail.com>.

// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.

// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

// This program is not written in literate form as the only purpose of this
// program is the _temporary_ creation of ELE the literate text editor and it
// will no longer be useful as the first version of ELE is ready. Nevertheless
// it will serve as an interesting exploration for the compiler in ELE and its
// file format.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define _POSIX_C_SOURCE 200809L

#define error(...) fprintf(stderr, "[ERROR]: " __VA_ARGS__)
#define system_error() perror("[ERROR]");
#define return_exit(r) do { return_value = r; goto exit; } while (0)

static char *
change_extension(char *file)
{
	size_t len = strlen(file);
	char *c = file + len;
	while (*c != '.' && c != file)
		c--;
	if (c != file) {
		/* c points to last . in filename */
		len = c - file;
	}

	char *r = (char *)malloc(len + 5);
	strncpy(r, file, len);
	r[len+0] = '.';
	r[len+1] = 'e';
	r[len+2] = 'l';
	r[len+3] = 'e';
	r[len+4] = '\0';
	return r;
}

int
main(int argc, char *argv[])
{
	int return_value = 0;
	FILE *f = NULL;
	FILE *out = NULL;

	if (argc < 2) {
		error("Insufficient arguments!\n");
		return_exit(1);
	}
	printf("Opening '%s'\n", argv[1]);
	f = fopen(argv[1], "r");
	if (!f) {
		error("Could not open file %s\n", argv[1]);
		system_error();
		return_exit(-1);
	}

	char *outfilename = change_extension(argv[1]);
	printf("Writting to '%s'\n", outfilename);
	out = fopen(outfilename, "wb");
	if (!out) {
		error("Could not open file %s\n", outfilename);
		system_error();
		return_exit(-1);
	}

exit:
	if (f) fclose(f);
	if (out) fclose(out);
	return return_value;
}
