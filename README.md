# ELE

ELE stands for Ernesto's Literate Environment. Literate programming is a new
paradigm proposed by Donald Knuth and used widely in his projects (see
[literateprogramming.com](http://literateprogramming.com)). "ele" is also the
name of the letter "L" in spanish so a future logo will expliot this
"coincidence".

## Description
ELE is a Literate programming system heavily inspired by
[Eve](https://witheve.com) and [Literate](https://zyedidia.github.io/literate/).
The system only works on one file because we not in the 90s anymore and
computers have more memory.

The tool is based on Markdown so it is easy to write on it and it produces
HTML output. However, it has some aditional features to allow better processing
and wrting (you can find more details about the aditional directives in
[Usage](#usage)).

The tool also has a mode in which the source file is a valid `C` program. This
is intentional so the only thing required to run the system is a valid `C`
compiler. However, in this mode you have to write the code in "compiler order"
for obvious reasons.

## Installation
To generate the executable just run
```terminal
$ gcc ele.c
```
NOTE: you can use any `C` compiler of your choice, simply change `gcc` by the
name of your compiler

## Usage

There is no manual written yet. And the syntaxis and macros are not yet defined
however, you can generate both the tangle and weave at the same time by running
```terminal
$ ./ele <youfile.ele>
```

## Roadmap
At the moment the roadmap is pretty vague but the ideas are the following
- [X] Create the ELE project
- [ ] Define the syntax used by ELE
- [ ] Create some unit testing
- [ ] Write the ELE manual
- [ ] Create a webpage for ELE
- [ ] Create an editor for ELE (See [https://gitlab.com/elancha/ete](ETE))
- [ ] Create a LSP for ELE

## Contributing
Just drop a pull request no fancy requirements are needed.

## Authors and acknowledgment
Here's a list of people that have contributed to the project in some form
sorted in alphabetical order:
- Ernesto Lanchares [elancha98@gmail.com](mailto:elancha98@gmail.com)

## License

Copyright 2022, Ernesto Lanchares
[elancha98@gmail.com](mailto:elancha98@gmail.com).

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
