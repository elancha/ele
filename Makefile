CFLAGS += -O3 -Wall -Wextra -std=c99 -pedantic -Wmissing-prototypes
CFLAGS += -Wstrict-prototypes -Wold-style-definition -g


program_creator: program_creator.c
	$(CC) $(CFLAGS) -o program_creator program_creator.c $(LDFLAGS)
